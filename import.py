#!/usr/bin/python
# -*- coding: utf-8 -*-
# © 2016 Jan Ole Zabel (joz)
# License: Do what the heck you want to, unless it's evil. (In fact WTFPL)
# Usage:   Run this program while having the unpacked gtfs-zip as working directory (e.g. cd /var/unpacked-gtfs). As parameter give the destination *.sqlite DB.

import sqlite3 as lite
import sys
import csv

def setup_table(name, Definition, cur):
	" Nach der Funktionsausführung existiert eine neue leere Tabelle mit der angegebenen Definition "
	cur.execute("DROP TABLE IF EXISTS " + name)
	cur.execute("CREATE TABLE " + name + Definition)

def read_csv_except_header(name, filename, fmtstr, cur):
	" Liest eine CSV in die Tabelle name ein. fmtstr (siehe Beispiele unten) besagt, welche Spalten Verwendung finden sollen und welche davon Strings sind ('') "
	filehandle = open(filename)
	filehandle.readline()	# first line is the header
	reader = csv.reader(filehandle)
	
	print "Inserting " + name
	for row in reader:
		cur.execute("INSERT INTO " + name + " VALUES " + fmtstr.format(*row))

con = None

try:
	# Connect
	con = lite.connect(sys.argv[1])
	con.isolation_level = None	# Damit wir commit ausführen können
	cur = con.cursor()

	cur.execute("BEGIN TRANSACTION")
	
	# Set up or empty the database
	setup_table("stops", "(stop_id INTEGER PRIMARY KEY, stop_name VARCHAR(100), stop_lat REAL, stop_lon REAL)", cur)
	setup_table("routes", "(route_id INTEGER PRIMARY KEY, agency_id VARCHAR(6), route_short_name VARCHAR(4), route_desc VARCHAR(4))", cur)
	setup_table("agencies", "(agency_id VARCHAR(6), agency_name VARCHAR(100), agency_url VARCHAR(32), agency_phone VARCHAR(21))", cur)
	setup_table("calendar_dates", "(service_id, date VARCHAR(8))", cur)
	setup_table("trips", "(route_id INTEGER, service_id INTEGER, trip_id INTEGER, trip_headsign VARCHAR(200))", cur)
	setup_table("stop_times", "(trip_id INTEGER, arrival_time VARCHAR(8), departure_time VARCHAR(8), stop_id INTEGER, stop_sequence INTEGER)", cur)
	
	# Import data from the gtfs files that hopefully lie in our working directory
	read_csv_except_header("stops", "stops.txt", "({0}, '{2}', {4}, {5})", cur)
	read_csv_except_header("routes", "routes.txt", "({0}, '{1}', '{2}', '{5}')", cur)
	read_csv_except_header("agencies", "agency.txt", "('{0}', '{1}', '{2}', '{3}')", cur)
	read_csv_except_header("calendar_dates", "calendar_dates.txt", "({0}, '{1}')", cur)
	read_csv_except_header("trips", "trips.txt", "({0}, {1}, {2}, '{3}')", cur)
	read_csv_except_header("stop_times", "stop_times.txt", "({0}, '{1}', '{2}', {3}, {4})", cur)
	
	print "Committing…"
	cur.execute("COMMIT")
finally:
	
	if con:
		con.close()
