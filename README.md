Installation
============

Für den Import in die Datenbank steht das Skript `import.py` bereit.
Für die Einrichtung des Spline-Dashboards (yet to create): `setup-system.sh`. Es richtet einen auf die Datenbank schreibenden und einen sie lesenden Benutzer ein. Der schreibende ist für Updates des GTFS-Datensatzes, der Lesende für das Fontend, das die Zeiten anzeigt.

Usage
=====

Derzeit existiert nur ein sehr simpel gestricktes Terminal-Frontend, `frontend-tput.sh` . Es zeigt noch keine relativen Zeiten an.
