#!/usr/bin/sh
# Parameter:
HALTESTELLE=$1	# Exakter Name wie in stops.txt
DATE=$2		# Tag
TIME_MIN=$3	# Zeit, Minimum
TIME_MAX=$4	# Zeit, obere Schranke
DB_PATH=$5	# Pfad zur *.sqlite-Datenbank
# Beispiel: abfrage.sh "Sandstr. (Berlin)" 20160319 17:01 18 "data/test.sqlite"
# findet alle Busse, die von der Haltestelle Sandstraße am 19. März 2016 zwischen einschließlich 17:01:00 Uhr und nicht einschließlich 18:00 Uhr abfahren.

# Eigentlicher Aufruf
sqlite3 "$DB_PATH" "SELECT departure_time, route_short_name, trip_headsign FROM stop_times
JOIN trips ON stop_times.trip_id = trips.trip_id
JOIN routes ON trips.route_id = routes.route_id
JOIN calendar_dates ON trips.service_id = calendar_dates.service_id
	WHERE stop_id = (SELECT stop_id FROM stops WHERE stop_name = '$HALTESTELLE')
	AND date = '$DATE'
	AND departure_time > '$TIME_MIN'
	AND departure_time < '$TIME_MAX'
ORDER BY departure_time;"
