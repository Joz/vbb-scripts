#!/bin/sh
set -e
groupadd vbb
useradd vbb-reader -g vbb
useradd vbb-writer -g vbb
mkdir -p /var/lib/vbb
chown -R vbb-writer:vbb /var/lib/vbb
chmod -R u+rw /var/lib/vbb
