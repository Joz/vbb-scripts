#!/bin/sh

set -e
trap finisch 1 2 3 6

finisch () {
	echo   UNTERBOCHEENN
	tput cnorm
	tput sgr0
	tput rc
}

tput civis # invisible cursor
tput sc

while sleep 5
do
	# Get the newest exciting infos
	tput cup 1 0
	HourPOne=$(expr $(date +%H) + 1)
	./abfrage.sh 'Heerstr./Wilhelmstr. (Berlin)' $(date "+%Y%m%d %H:%M $HourPOne:%M") /var/lib/vbb/data.sqlite > /tmp/vbb.txt # Wait until the query finishes, do not print immediately

	# Header
	clear
	tput setab 2
	tput setaf 0
	echo "Abfahrten der nächsten Stunde       'Heerstr./Wilhelmstr. (Berlin)'"
	tput sgr0

	# Print the times out
	sed 's/|/	/g' < /tmp/vbb.txt | head -n $(expr $(tput lines) - 1)
	tput sc
done
